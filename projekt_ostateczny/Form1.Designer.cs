﻿namespace projekt_ostateczny
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_n1 = new System.Windows.Forms.TextBox();
            this.textBox_n2 = new System.Windows.Forms.TextBox();
            this.textBox_n3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_n4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_koszt_czekania_studenci = new System.Windows.Forms.TextBox();
            this.checkBox_odroznialni = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_s1 = new System.Windows.Forms.TextBox();
            this.textBox_s2 = new System.Windows.Forms.TextBox();
            this.textBox_s3 = new System.Windows.Forms.TextBox();
            this.textBox_s4 = new System.Windows.Forms.TextBox();
            this.textBox_m1 = new System.Windows.Forms.TextBox();
            this.textBox_m2 = new System.Windows.Forms.TextBox();
            this.textBox_m3 = new System.Windows.Forms.TextBox();
            this.textBox_m4 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox_godz = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox_koszt_czekania_wykladowcy = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox_wielkosc_probki = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button_uruchom = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox_wyniki = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.TextBox_czasy = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_n1
            // 
            this.textBox_n1.Location = new System.Drawing.Point(49, 30);
            this.textBox_n1.Name = "textBox_n1";
            this.textBox_n1.Size = new System.Drawing.Size(25, 20);
            this.textBox_n1.TabIndex = 1;
            // 
            // textBox_n2
            // 
            this.textBox_n2.Location = new System.Drawing.Point(49, 56);
            this.textBox_n2.Name = "textBox_n2";
            this.textBox_n2.Size = new System.Drawing.Size(24, 20);
            this.textBox_n2.TabIndex = 2;
            // 
            // textBox_n3
            // 
            this.textBox_n3.Location = new System.Drawing.Point(49, 82);
            this.textBox_n3.Name = "textBox_n3";
            this.textBox_n3.Size = new System.Drawing.Size(24, 20);
            this.textBox_n3.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "5 - 4.5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "4 -3.5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "3";
            // 
            // textBox_n4
            // 
            this.textBox_n4.Location = new System.Drawing.Point(49, 108);
            this.textBox_n4.Name = "textBox_n4";
            this.textBox_n4.Size = new System.Drawing.Size(24, 20);
            this.textBox_n4.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBox_koszt_czekania_studenci);
            this.groupBox1.Controls.Add(this.checkBox_odroznialni);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox_s1);
            this.groupBox1.Controls.Add(this.textBox_s2);
            this.groupBox1.Controls.Add(this.textBox_s3);
            this.groupBox1.Controls.Add(this.textBox_s4);
            this.groupBox1.Controls.Add(this.textBox_m1);
            this.groupBox1.Controls.Add(this.textBox_m2);
            this.groupBox1.Controls.Add(this.textBox_m3);
            this.groupBox1.Controls.Add(this.textBox_m4);
            this.groupBox1.Controls.Add(this.textBox_n1);
            this.groupBox1.Controls.Add(this.textBox_n2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox_n3);
            this.groupBox1.Controls.Add(this.textBox_n4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(155, 195);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Studenci";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 143);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "odróżnialni";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "koszt czekania";
            // 
            // textBox_koszt_czekania_studenci
            // 
            this.textBox_koszt_czekania_studenci.Location = new System.Drawing.Point(92, 163);
            this.textBox_koszt_czekania_studenci.Name = "textBox_koszt_czekania_studenci";
            this.textBox_koszt_czekania_studenci.Size = new System.Drawing.Size(22, 20);
            this.textBox_koszt_czekania_studenci.TabIndex = 22;
            this.textBox_koszt_czekania_studenci.Text = "1";
            // 
            // checkBox_odroznialni
            // 
            this.checkBox_odroznialni.AutoSize = true;
            this.checkBox_odroznialni.Checked = true;
            this.checkBox_odroznialni.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_odroznialni.Location = new System.Drawing.Point(92, 143);
            this.checkBox_odroznialni.Name = "checkBox_odroznialni";
            this.checkBox_odroznialni.Size = new System.Drawing.Size(15, 14);
            this.checkBox_odroznialni.TabIndex = 21;
            this.checkBox_odroznialni.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox_odroznialni.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(114, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(114, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "σ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(86, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "μ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "N";
            // 
            // textBox_s1
            // 
            this.textBox_s1.Location = new System.Drawing.Point(111, 30);
            this.textBox_s1.Name = "textBox_s1";
            this.textBox_s1.Size = new System.Drawing.Size(38, 20);
            this.textBox_s1.TabIndex = 13;
            this.textBox_s1.Text = "0,15";
            // 
            // textBox_s2
            // 
            this.textBox_s2.Location = new System.Drawing.Point(111, 56);
            this.textBox_s2.Name = "textBox_s2";
            this.textBox_s2.Size = new System.Drawing.Size(38, 20);
            this.textBox_s2.TabIndex = 14;
            this.textBox_s2.Text = "0,15";
            // 
            // textBox_s3
            // 
            this.textBox_s3.Location = new System.Drawing.Point(111, 82);
            this.textBox_s3.Name = "textBox_s3";
            this.textBox_s3.Size = new System.Drawing.Size(38, 20);
            this.textBox_s3.TabIndex = 15;
            this.textBox_s3.Text = "0,15";
            // 
            // textBox_s4
            // 
            this.textBox_s4.Location = new System.Drawing.Point(111, 108);
            this.textBox_s4.Name = "textBox_s4";
            this.textBox_s4.Size = new System.Drawing.Size(38, 20);
            this.textBox_s4.TabIndex = 16;
            this.textBox_s4.Text = "0,15";
            // 
            // textBox_m1
            // 
            this.textBox_m1.Location = new System.Drawing.Point(80, 30);
            this.textBox_m1.Name = "textBox_m1";
            this.textBox_m1.Size = new System.Drawing.Size(25, 20);
            this.textBox_m1.TabIndex = 9;
            this.textBox_m1.Text = "2,3";
            // 
            // textBox_m2
            // 
            this.textBox_m2.Location = new System.Drawing.Point(80, 56);
            this.textBox_m2.Name = "textBox_m2";
            this.textBox_m2.Size = new System.Drawing.Size(24, 20);
            this.textBox_m2.TabIndex = 10;
            this.textBox_m2.Text = "2,7";
            // 
            // textBox_m3
            // 
            this.textBox_m3.Location = new System.Drawing.Point(80, 82);
            this.textBox_m3.Name = "textBox_m3";
            this.textBox_m3.Size = new System.Drawing.Size(24, 20);
            this.textBox_m3.TabIndex = 11;
            this.textBox_m3.Text = "3,0";
            // 
            // textBox_m4
            // 
            this.textBox_m4.Location = new System.Drawing.Point(80, 108);
            this.textBox_m4.Name = "textBox_m4";
            this.textBox_m4.Size = new System.Drawing.Size(24, 20);
            this.textBox_m4.TabIndex = 12;
            this.textBox_m4.Text = "3,4";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.textBox_godz);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.textBox_koszt_czekania_wykladowcy);
            this.groupBox2.Location = new System.Drawing.Point(167, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(145, 100);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wykładowca";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "godzina";
            // 
            // textBox_godz
            // 
            this.textBox_godz.Location = new System.Drawing.Point(54, 35);
            this.textBox_godz.Name = "textBox_godz";
            this.textBox_godz.Size = new System.Drawing.Size(58, 20);
            this.textBox_godz.TabIndex = 26;
            this.textBox_godz.Text = "9:00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "koszt czekania";
            // 
            // textBox_koszt_czekania_wykladowcy
            // 
            this.textBox_koszt_czekania_wykladowcy.Location = new System.Drawing.Point(90, 61);
            this.textBox_koszt_czekania_wykladowcy.Name = "textBox_koszt_czekania_wykladowcy";
            this.textBox_koszt_czekania_wykladowcy.Size = new System.Drawing.Size(22, 20);
            this.textBox_koszt_czekania_wykladowcy.TabIndex = 24;
            this.textBox_koszt_czekania_wykladowcy.Text = "5";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBox_wielkosc_probki);
            this.groupBox3.Location = new System.Drawing.Point(169, 114);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(143, 58);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Program";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "wielkość próbki";
            // 
            // textBox_wielkosc_probki
            // 
            this.textBox_wielkosc_probki.Location = new System.Drawing.Point(89, 15);
            this.textBox_wielkosc_probki.Name = "textBox_wielkosc_probki";
            this.textBox_wielkosc_probki.Size = new System.Drawing.Size(25, 20);
            this.textBox_wielkosc_probki.TabIndex = 0;
            this.textBox_wielkosc_probki.Text = "600";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(332, 241);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button_uruchom);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(324, 215);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ustawienia";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button_uruchom
            // 
            this.button_uruchom.Location = new System.Drawing.Point(204, 185);
            this.button_uruchom.Name = "button_uruchom";
            this.button_uruchom.Size = new System.Drawing.Size(75, 23);
            this.button_uruchom.TabIndex = 12;
            this.button_uruchom.Text = "Uruchom";
            this.button_uruchom.UseVisualStyleBackColor = true;
            this.button_uruchom.Click += new System.EventHandler(this.button2_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox_wyniki);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(324, 215);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Podsumowanie";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox_wyniki
            // 
            this.textBox_wyniki.Location = new System.Drawing.Point(0, 0);
            this.textBox_wyniki.Name = "textBox_wyniki";
            this.textBox_wyniki.Size = new System.Drawing.Size(324, 219);
            this.textBox_wyniki.TabIndex = 0;
            this.textBox_wyniki.Text = "";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.TextBox_czasy);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(324, 215);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Harmonogram";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // TextBox_czasy
            // 
            this.TextBox_czasy.Location = new System.Drawing.Point(-3, 0);
            this.TextBox_czasy.Name = "TextBox_czasy";
            this.TextBox_czasy.Size = new System.Drawing.Size(327, 215);
            this.TextBox_czasy.TabIndex = 0;
            this.TextBox_czasy.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 244);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Harmonogram";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_n1;
        private System.Windows.Forms.TextBox textBox_n2;
        private System.Windows.Forms.TextBox textBox_n3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_n4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_s1;
        private System.Windows.Forms.TextBox textBox_s2;
        private System.Windows.Forms.TextBox textBox_s3;
        private System.Windows.Forms.TextBox textBox_s4;
        private System.Windows.Forms.TextBox textBox_m1;
        private System.Windows.Forms.TextBox textBox_m2;
        private System.Windows.Forms.TextBox textBox_m3;
        private System.Windows.Forms.TextBox textBox_m4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox_odroznialni;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_koszt_czekania_studenci;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox_koszt_czekania_wykladowcy;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox_wielkosc_probki;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button_uruchom;
        private System.Windows.Forms.TextBox textBox_godz;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RichTextBox textBox_wyniki;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox TextBox_czasy;
    }
}

