﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SolverFoundation;
using Microsoft.SolverFoundation.Services;
namespace projekt_ostateczny
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

           
        }

        private void button2_Click(object sender, EventArgs e)
        {   //wczytywanie danych z formy wszelkie oznaczenia przyjmujemy za praca podana z bibliografii
           int rozmiar_probki = Convert.ToInt32(textBox_wielkosc_probki.Text);
           double c_w = Convert.ToDouble(textBox_koszt_czekania_studenci.Text);
           double c_s = Convert.ToDouble(textBox_koszt_czekania_wykladowcy.Text); 
           double c_l = 0; // koszt spoznienia (na razie nie przewidujemy)
           Kategoria[] studenci = new Kategoria[4]; // zapiszemy informacje o grupach

            studenci[0] = new Kategoria(int_or_0(textBox_n1.Text), Convert.ToDouble(textBox_m1.Text), Convert.ToDouble(textBox_s1.Text));
            studenci[1] = new Kategoria(int_or_0(textBox_n2.Text), Convert.ToDouble(textBox_m2.Text), Convert.ToDouble(textBox_s2.Text));
            studenci[2] = new Kategoria(int_or_0(textBox_n3.Text), Convert.ToDouble(textBox_m3.Text), Convert.ToDouble(textBox_s3.Text));
            studenci[3] = new Kategoria(int_or_0(textBox_n4.Text), Convert.ToDouble(textBox_m4.Text), Convert.ToDouble(textBox_s4.Text));
            int n = studenci[0].n+studenci[1].n+studenci[2].n+studenci[3].n; //ile studentow
            if (n == 0) {
                tabControl1.SelectedIndex = 2;
                TextBox_czasy.Text = "Liczba studentów wynosi 0 - egzamin nie odbędzie się";
                return;
            }
            int d = 10000; // ile mamy czasu dostepnego
          
           // wczytujemy godzine by ladnie wyliczyc potem czas
            TimeSpan[] czasy_przyjscia = new TimeSpan[n];
            String[] czasy = textBox_godz.Text.Split(':');
            TimeSpan poczatek = new TimeSpan(Convert.ToInt32(czasy[0]), Convert.ToInt32(czasy[1]), 0);
            czasy_przyjscia[0] = poczatek;

            SolverContext context = SolverContext.GetContext();
            context.ClearModel();
            Model model = context.CreateModel();

            //tworzymy nasze zmienne decyzyjne
            Set se = new Set(Domain.RealNonnegative, "se"); //zbior do ktorego sa przypisane wszystkie parametry
            Decision x = new Decision(Domain.RealNonnegative, "x", se); // czas jaki przydzielamy na odpowiedz
            model.AddDecision(x);
            //drugiego rzedu
            RecourseDecision w = new RecourseDecision(Domain.RealNonnegative, "w", se);
            RecourseDecision s = new RecourseDecision(Domain.RealNonnegative, "s", se);
            RecourseDecision l = new RecourseDecision(Domain.RealNonnegative, "l");
            RecourseDecision g = new RecourseDecision(Domain.RealNonnegative, "g");

            //losowe parametry
            LogNormalDistributionParameter Z = new LogNormalDistributionParameter("Z", se);
            int[] remaining = new int[4];
            for (int i = 0; i < 4; i++)
                remaining[i] = studenci[i].n;
            // tablica srednich i odchylen
            int[] srednie = new int[n];
            int[] odchyl = new int[n]; 
            // tu uzuplenic
            Parametry[] parametry = new Parametry[n];
            int[] oceny = new int[n];
            int j;
            bool odroznialni = checkBox_odroznialni.Checked;
            if (odroznialni)  // jeśli jest zaznaczone ze studenci sa odroznialni to dla kazdego przypisujemy inne czasy
            {
                //j = -1; // nalezy ten fragment odkomentowac dla opccji przyporzadkowania na przemian i zmienic nazwe funckji
                for (int i = 0; i < n; i++)
                {
                    j = podaj_indeks(remaining); //,j); // to tez odkomentowac
                    oceny[i] = j;
                    parametry[i] = new Parametry(studenci[j].m, studenci[j].s, i);
                }
            }
            else {
                double sigma_nieodr = 0.3;
                double sred_nieodr = (studenci[0].n*10+studenci[1].n*15+studenci[2].n*20+studenci[3].n*30)/n;
                double mi_nieodr = Math.Log(sred_nieodr) - sigma_nieodr * sigma_nieodr / 2;
                for (int i = 0; i < n; i++)
                {
                    parametry[i] = new Parametry(mi_nieodr, sigma_nieodr, i);
                }
            }

            //var parametry = Enumerable.Range(0, n).Select(item => new { meanLog = srednie[item],  sdLog = odchyl[item], Id = item });
            //IEnumerable<Parametry> parametry;

            Z.SetBinding(parametry, "meanLog", "sdLog", "index");
            model.AddParameter(Z);
            model.AddDecisions(w, l, s, g);

            //teraz tylko zapisac ograniczenia
            model.AddConstraint("c0", x[n - 1] == 0);
            model.AddConstraint("c0_1", w[0] == 0); // 1-sza osoba nie czeka
            model.AddConstraint("c0_2", s[0] == 0); // na 1-sza osobe sie nie czeka

            model.AddConstraint("c2", Model.ForEachWhere(se, i => -w[i] + w[i + 1] - s[i + 1] == Z[i] - x[i], i => i < n - 1)); // wiadomo ze i idzie od  
            model.AddConstraint("c3", -w[n - 1] + l - g == Z[n - 1] - d + Model.Sum(Model.ForEachWhere(se, i => x[i], i => i < n - 1)));

            model.AddGoal("goal1", GoalKind.Minimize, Model.Sum(Model.ForEach(se, i => c_w * w[i] + c_s * s[i])) + c_l * l);
            context.SamplingParameters.SampleCount = rozmiar_probki;
            Solution solution = context.Solve();
            Report report = solution.GetReport();
            textBox_wyniki.Text = report.ToString();
            tabControl1.SelectedIndex = 2;
            TextBox_czasy.Text = "1: " + czasy_przyjscia[0].ToString() + opis_oceny(oceny[0]) + "\n";
            foreach (var dec in solution.Decisions) {
                int k = 1;
                foreach (double czas in dec.GetValuesByIndex())
                {
                    if (k < n)
                    {
                        czasy_przyjscia[k] = czasy_przyjscia[k - 1] + new TimeSpan(0, (int)Math.Floor(czas), 0);

                        TextBox_czasy.Text += (k + 1).ToString() + ": " + czasy_przyjscia[k].ToString() + (odroznialni ? opis_oceny(oceny[k]) : "") + "\n";
                        k++;
                    }
                }
            }
        }
        // od najlepszej oceny do najgorszej
        int podaj_indeks(int[] remaining) // podaje z ktorej grupy bierzemy nastepnego studenta dla kazdej z grup pozostalo tyle osob ile jest w tablicy remaining
        {
            int j = 0;
            while (remaining[j] == 0) {
                j ++;
            }
            remaining[j] = remaining[j] - 1;
            return j;
                
        }
        // od najgorszej oceny do najlepszej
        int podaj_indeks_odw(int[] remaining) // podaje z ktorej grupy bierzemy nastepnego studenta dla kazdej z grup pozostalo tyle osob ile jest w tablicy remaining
        {
            int j = 3;
            while (remaining[j] == 0)
            {
                j--;
            }
            remaining[j] = remaining[j] - 1;
            return j;

        }

        int podaj_indeks_alt(int[] remaining, int ostatnio_uzywany) // podaje z ktorej grupy bierzemy nastpenego studenta dla kazdej z grup pozostalo tyle osob ile jest w tablicy remaining
        {
            int j = (ostatnio_uzywany + 1)%4;
            while (remaining[j] == 0)
            {
                j=(j+1)%4;
            }
            remaining[j] = remaining[j] - 1;
            return j;

        }

        string opis_oceny(int i) {

            switch (i)
            { case 0:
                    return " | 4,5 - 5,0";
              case 1:
                    return " | 4,0 - 3,5";
              case 2:
                    return " | 3.0";
              case 3:
                    return " | 2.0";

            
            }
            return "błąd!";
        }

        int int_or_0(string text)
        {
            try
            {
                return Convert.ToInt32(text);
            }
            catch (Exception)
            {
                return 0;
            }
        }

       

    
    }
}
