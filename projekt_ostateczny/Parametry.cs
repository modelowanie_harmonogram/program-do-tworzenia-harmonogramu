﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projekt_ostateczny
{
    class Parametry
    {
        public double meanLog;
        public double sdLog;
        public int index;

        public Parametry(double m, double s, int i)
        {
            meanLog = m;
            sdLog = s;
            index = i;
        }
    }

}
